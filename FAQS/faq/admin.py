from django.contrib import admin
from django.db import models

from .models import Question,Answer

# class AnswerInline(admin.StackedInline):
#     model = Answer
#     extra = 1

class QuestionAdmin(admin.ModelAdmin):
    # inlines = [AnswerInline]

    list_display = ('question_text','pub_date','created_by')

admin.site.register(Question,QuestionAdmin)
admin.site.register(Answer)