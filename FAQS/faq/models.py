from django.db import models
from django.urls import reverse
from django.contrib.auth.models import User



class Question(models.Model):
    question_text = models.CharField(max_length=200)
    description = models.TextField(blank=True)
    created_by = models.ForeignKey(User, on_delete=models.PROTECT)
    pub_date = models.DateTimeField(auto_now=True)
    
    def __str__(self):
        return self.question_text

    def get_absolute_url(self):
        return reverse('faq:question_detail', kwargs={'pk': self.pk})

    # answercount = models.IntegerField(default=0)

    # @property
    # def _get_answer_count(self):
    #     """return answer count"""
    #     return self.answer_set.count()

    # def save(self, force_insert, force_update, using, update_fields):
    #     self.answercount = self._get_answer_count()
    #     return super().save(force_insert=force_insert, force_update=force_update, using=using, update_fields=update_fields)
    # answercount = property(_get_answer_count)
    # answercount = 

class Answer(models.Model):
    # question = models.ForeignKey(Question, related_name='answers', on_delete=models.CASCADE)
    question = models.ForeignKey(Question, on_delete=models.CASCADE)
    answer_text = models.TextField(blank=False)
    created_by = models.ForeignKey(User, on_delete=models.CASCADE)
    pub_date = models.DateTimeField(auto_now=True)