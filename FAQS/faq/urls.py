from django.db import router
from django.urls.conf import path,include

from rest_framework.routers import DefaultRouter

# pure django
from .views import IndexViews,QuestionDetail,QuestionCreate,AnswerQuestion

# rest api
from .apiviews import QuestionViewSet,AnswerList,LoginView,AnswerCreate,UserCreate

router = DefaultRouter()

router.register('faqapi',QuestionViewSet,basename='faqapi')

app_name = "faq"

urlpatterns = [
    # api login
    path('login/', LoginView.as_view(), name='login'),

    path('users/create', UserCreate.as_view(), name="user_create"),
    path('faqapi/<int:pk>/answers/', AnswerList.as_view(),name='answer_list'),
    path('faqapi/<int:pk>/answers/create/', AnswerCreate.as_view(),name='answer_create'),
    # path()
    path('faq/', IndexViews.as_view(), name='index'),
    path('faq/create/', QuestionCreate.as_view(), name='question_create'),
    path('faq/<int:pk>/', QuestionDetail.as_view(),name='question_detail'),
    path('faq/<int:question_id>/answers/create/', AnswerQuestion.as_view(), name='answer_question')
    
]

urlpatterns += router.urls