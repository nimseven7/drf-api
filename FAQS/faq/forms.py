from django import forms

class AnswerForm(forms.Form):
    answer_text = forms.CharField(widget=forms.Textarea)