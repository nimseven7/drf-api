# from rest_framework import generics, serializers
from rest_framework import generics
from rest_framework import status,viewsets
from rest_framework.response import Response
from django.shortcuts import get_object_or_404
from rest_framework.views import APIView
# from rest_framework.response import Response
from django.contrib.auth import authenticate
from rest_framework.exceptions import PermissionDenied


from .models import Question, Answer
from .serializers import QuestionSerializer, AnswerSerializer, UserSerializer


class LoginView(APIView):
    permission_classes = ()

    def post(self, request,):
        username = request.data.get('username')
        password = request.data.get('password')
        user = authenticate(username=username,password=password)
        if user:
            return Response({"token": user.auth_token.key})
        else:
            return Response({"error": "Wrong Credentials"},status=status.HTTP_400_BAD_REQUEST)

class UserCreate(generics.CreateAPIView):
    authentication_classes = ()
    permission_classes = ()
    serializer_class = UserSerializer

    


# regroup same serializer_class in a viewsets
class QuestionViewSet(viewsets.ModelViewSet):
    queryset = Question.objects.all()
    def destroy(self, request, *args, **kwargs):
        question = Question.objects.get(pk=self.kwargs['pk'])
        if not request.user == question.created_by:
            raise PermissionDenied("You can only delete a question you created")
        return super().destroy(request, *args, **kwargs)
    
    # def create(self, request, *args, **kwargs):
    #     return super().create(request, *args, **kwargs)

    serializer_class = QuestionSerializer

# class AnswerList(generics.ListCreateAPIView):
class AnswerList(generics.ListAPIView):
    def get_queryset(self):
        queryset = Answer.objects.filter(question_id=self.kwargs["pk"])
        return queryset

    # def create(self, request, *args, **kwargs):
    #     question = get_object_or_404(Question,pk=self.kwargs['pk'])
    #     created_by = request.POST.get('created_by')
    #     if created_by is None:
    #         raise PermissionDenied(request.body)
    #     return super().create(request, *args, **kwargs)
    
    # def destroy(self, request, *args, **kwargs):
    #     answer = Answer.objects.get(pk=self.kwargs['pk'])
    #     if not request.user == answer.created_by:
    #         raise PermissionDenied("You can only delete answer you created")
    #     return super().post(request, *args, **kwargs)

    serializer_class = AnswerSerializer

class AnswerCreate(APIView):
    def post(self, request, pk):
        # print(request.user)
        created_by = request.POST.get('created_by')
        if request.user.id != int(created_by):
            return Response(data={"error":"Credential not corresponding with the 'created_by' value"},status=status.HTTP_400_BAD_REQUEST)
        question_id = request.POST.get('question')
        if question_id is None:
            return Response(data={"error":"question is missing"},status=status.HTTP_400_BAD_REQUEST)
        if pk != int(question_id):
            return Response(data={"error": "question id in post request does not match the URL question id"},status=status.HTTP_400_BAD_REQUEST)

        datas = AnswerSerializer(data={"question": question_id,"created_by":created_by,"answer_text":request.POST.get('answer_text')})
        print(datas)
        print(datas.is_valid())
        return Response(data={"pk":pk,"question":question_id})