# from django import views
# from django.db import models
from django.http.response import HttpResponse, HttpResponseRedirect
from django.shortcuts import get_list_or_404,get_object_or_404, redirect,render
from django.views import generic
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views import View

from django.db.models import Count,Q


from .models import Question,Answer
from .serializers import QuestionSerializer,AnswerSerializer

# these views are for public content

class IndexViews(generic.ListView):
    template_name = "faq/index.html"
    context_object_name = "latest_question_list"

    paginate_by = 10
    def get_queryset(self):
        # return super().get_queryset()
        if self.request.GET.get('answered'):
        
            # print("q izy")
            mquestions = Question.objects.annotate(num_answers=Count('answer'))
            q = mquestions.filter(num_answers__gte=1).order_by('pub_date')
            return q
        else:
            # print("q tsy izy")
            return Question.objects.order_by('-pub_date')
            
        #for filter purpose
        # return 


# def index(request):
#     # return HttpResponse("index here")
#     questions = Question.objects.order_by('-pub_date')[:5]
#     return render(request,"faq/index.html",{})

    # def get(self, request, *args, **kwargs):
    #     print(args,kwargs,request.GET.get('answered'))
    #     if request.GET.get('answered'):
    #     return super().get(request, *args, **kwargs)
    

class QuestionDetail(generic.DetailView):
    model = Question
    template_name = "faq/detail.html"

    


class QuestionCreate(LoginRequiredMixin,generic.CreateView):
# class QuestionCreate(generic.CreateView):
    model = Question
    fields = ["question_text","description"]
    # fields = "__all__"
    template_name = "faq/submit_question.html"

    def form_valid(self, form):
        form.instance.created_by = self.request.user
        return super().form_valid(form)

    
# class AnswerCreate(LoginRequiredMixin,generic.CreateView):
#     model = Answer
#     fields = ["answer_text"]
#     template_name = "faq/detail.html"

# def answer_question(request, question_id):
#     question = get_object_or_404(Question,pk=question_id)
#     print(question)
#     # return redirect(Question.objects.get(pk=question_id).get_absolute_url())

class AnswerQuestion(LoginRequiredMixin,View):
    
    login_url = 'accounts/login/'
    redirect_field_name = 'redirect_to'

    def post(self,request,question_id):
        created_by = request.user.id
        answer_text = request.POST.get("answer_text")
        print(created_by,answer_text)

        serializer = AnswerSerializer(data={"question":question_id,"answer_text":answer_text,"created_by":created_by})
        if(serializer.is_valid()):
            serializer.save()

        return redirect(Question.objects.get(pk=question_id).get_absolute_url())
        

    # needed for allowing GET
    def get(self,request, question_id):
        # render(request, "faq/detail.html")
        redirect(Question.objects.get(pk=question_id).get_absolute_url())